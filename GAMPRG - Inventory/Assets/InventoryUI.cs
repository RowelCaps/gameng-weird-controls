﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] private GameObject slotPrefab;
    [SerializeField] private Inventory inventory;
    [SerializeField] private int maxDisplaySLot;

    private List<GameObject> slot = new List<GameObject>();

    private void Awake()
    {
        for (int i = 0; i < maxDisplaySLot; i++)
        {
            GameObject sl = Instantiate(slotPrefab, transform);
            sl.GetComponent<Slot>().inventory = inventory;
            sl.GetComponent<Slot>().index = i;
            sl.GetComponent<Slot>().AddListeners();
            sl.GetComponent<Slot>().UpdateUI(inventory);
            slot.Add(sl);
        }
    }
}
