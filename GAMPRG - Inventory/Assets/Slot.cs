﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    [SerializeField] Image icon;
    [SerializeField] Text stackText;

    public Inventory inventory { get; set; }
    public int index { get; set; }

    public void UpdateUI(Inventory inventory)
    {
        Item item = inventory.GetItem(index);

        if (item == null)
        {
            stackText.gameObject.SetActive(false);
            icon.sprite = null;
            return;
        }

        if (!item.IsStackable)
            stackText.gameObject.SetActive(false);
        else
        {
            stackText.gameObject.SetActive(true);
            stackText.text = inventory.GetStackCountAtIndex(index).ToString();
        }
        icon.sprite = item.GetItemSprite;
    }

    public void AddListeners()
    {
        inventory.AddListenerOnItemAdded(UpdateUI);
        inventory.AddListenerOnItemDiscard(UpdateUI);
        inventory.AddListenerOnItemFull(UpdateUI);
    }
}
