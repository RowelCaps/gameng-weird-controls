﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int maxHealth = 100;

    private int currentHealth = 0;

    public void AddHealth(int value)
    {
        currentHealth += value;
    }
}
