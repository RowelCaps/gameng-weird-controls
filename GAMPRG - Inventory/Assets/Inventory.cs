﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using System.Linq;

public class InventoryEvent : UnityEvent<Inventory> { }

public class Inventory : MonoBehaviour
{
    [SerializeField] private int maxSlot = 20;

    [Header("Items added in invetory on start scene")]
    [SerializeField] private List<ItemStack> itemAddedOnPlay;

    [Header("Events when any item would be added/discarded/full")]
    [SerializeField] private InventoryEvent OnItemAdded = new InventoryEvent();
    [SerializeField] private InventoryEvent OnItemDiscard = new InventoryEvent();
    [SerializeField] private InventoryEvent OnItemFull = new InventoryEvent();

    List<ItemStack> itemCache = new List<ItemStack>();

    public int GetOccupiedSlotCount { get { return itemCache.Count; } }
    public bool IsSlotFull { get { return itemCache.Count >= maxSlot; } }
    public int GetMaxSlot { get { return maxSlot; } }

    private void Start()
    {
        foreach(ItemStack item in itemAddedOnPlay)
        {
            Item it = Instantiate(item.item);
            item.item = it;
            itemCache.Add(item);
            item.item.gameObject.SetActive(false);
        }
    }

    //returns the exact item in the index parameter otherwise null
    public Item GetItem(int p_index)
    {
        return p_index >= itemCache.Count? null : itemCache[p_index].item;
    }

    //returns the first item it finds with the passed name parameter  otherwise null
    public Item GetItem(string p_itemName)
    {
        Item item = itemCache.Find(x => x.item.ItemName == p_itemName).item;

        return item;
    }

    public bool ContainsItem(Item item)
    {
        return itemCache.Exists(x => x.item.ItemName == item.ItemName);
    }

    public int GetStackCountAtIndex(int p_index)
    {
        return itemCache[p_index].stackCount;
    }

    //get total of item in all of the slots
    public int GetTotalItemCountOf(string p_itemName)
    {
        ItemStack[] stack = itemCache.FindAll(x => x.item.ItemName == p_itemName).ToArray();

        int count = 0;

        foreach (ItemStack it in stack)
        {
            count += it.stackCount;
        }

        return count;
    }

    //Add item
    //OnItemAdded would be invoke once this function is called
    public void AddItem(Item p_item)
    {
        ItemStack[] items = itemCache.FindAll(x => x.item.ItemName == p_item.ItemName).ToArray<ItemStack>();
        print("hey");
        if (maxSlot <= itemCache.Count)
            return;

        ItemStack item = null;
        bool hasValidItem = false;

        if (items.Length == 0 || !p_item.IsStackable)
        {
            item = new ItemStack();
            item.item = Instantiate(p_item);
            itemCache.Add(item);
        }
        else
        {
            foreach (ItemStack it in items)
            {
                if (it.stackCount < p_item.GetMaxStack)
                {
                    item = it;
                    hasValidItem = true;
                }
            }

            if(!hasValidItem)
            {
                item = new ItemStack();
                item.item = Instantiate(p_item);
                itemCache.Add(item);
            }
        }

        ++item.stackCount;

        item.item.OnItemAdded.Invoke();
        OnItemAdded.Invoke(this);
        item.item.currentInventoryStashed = this;

        if (IsSlotFull)
            OnItemFull.Invoke(this);
    }

    //use item once at index of.
    public void UseItem(int p_index)
    {
        ItemStack item = itemCache[p_index];
        IUsableItem usableItem = item.item.GetComponent<IUsableItem>();

        if (usableItem == null)
            return;

        usableItem.OnUseItem(this);
        DiscardItem(item.item);
    }

    //Will use all items with the name p_itemName
    public void UseAllItemWithNameOf(string p_itemName)
    {
        ItemStack[] item = itemCache.FindAll(x => x.item.ItemName == p_itemName).ToArray<ItemStack>();

        if (item[0].item.GetComponent<IUsableItem>() == null)
            return;

        for (int i = 0; i < item.Length; i++)
        {
            int stackCount = item[i].stackCount;
            for (int j = 0; j < stackCount; j++)
            {
                int index = itemCache.IndexOf(item[i]);
                UseItem(index);
            }
        }
    }

    //use all stacked item at a certain index
    public void UseAllItemAtIndexOf(int p_index)
    {
        Assert.IsTrue(p_index < itemCache.Count, "Use All Item index Error, Index out of range");

        ItemStack item = itemCache[p_index];

        if (item.item.GetComponent<IUsableItem>() == null)
            return;


        int stackCount = item.stackCount;

        for (int i = 0; i < stackCount; i++)
        {
            UseItem(p_index);
        }
    }

    //Discard item using index of   
    //--------If there is stack, stack will decrease but it will not totally remove the item------
    //-------- OnItemDiscard would be invoke --------------
    public void DiscardItem(int p_index)
    {
        Assert.IsTrue(p_index < itemCache.Count, "index out of range, forgot to check something?");

        ItemStack stack = itemCache[p_index];

        if (stack.stackCount <= 1)
        {
            itemCache.RemoveAt(p_index);
            stack.item.currentInventoryStashed = null;
            Destroy(stack.item.gameObject);
        }
        else
            --stack.stackCount;

        OnItemDiscard.Invoke(this);
    }

    //Discard Item by name  ----- just shortcut if you want to pass the name only------------
    //--------If there is stack, stack will decrease but it will not totally remove the item------
    //-------- OnItemDiscard would be invoke --------------
    public void DiscardItem(string p_itemName)
    {
        ItemStack stack = itemCache.Find(x => x.item.ItemName == p_itemName);
        Assert.IsNotNull(stack, "No " + p_itemName + " found on discard item");

        DiscardItem(itemCache.IndexOf(stack));
    }

    //Discard by item ----- just shortcut if you want to pass the whole item ------------
    //--------If there is stack, stack will decrease but it will not totally remove the item------
    //-------- OnItemDiscard would be invoke --------------
    public void DiscardItem(Item item)
    {
        DiscardItem(item.ItemName);
    }

    //Discard all items at index off 
    //--------All stack would be lose but they would still run all their OnDiscard Event--------
    public void DiscardAllItemAtIndexOf(int p_index)
    {
        Assert.IsTrue(p_index < itemCache.Count, "Discard All Item Error, Index out of range");

        ItemStack stack = itemCache[p_index];
        int stackCount = stack.stackCount;

        for(int i = 0; i < stackCount; i++)
        {
            DiscardItem(p_index);
            print("discard");
        }
    }

    // Add Listener for OnItemAdded
    // ------------Did not public the event because it should not be set outside this script-------
    public void AddListenerOnItemAdded(UnityAction<Inventory> action)
    {
        OnItemAdded.AddListener(action);
    }

    // Add Listener for OnItemDiscard
    public void AddListenerOnItemDiscard(UnityAction<Inventory> action)
    {
        OnItemDiscard.AddListener(action);
    }

    // Add Listener for OnItemFull
    public void AddListenerOnItemFull(UnityAction<Inventory> action)
    {
        OnItemFull.AddListener(action);
    }

    [System.Serializable]
    private class ItemStack
    {
        public Item item;
        public int stackCount = 0;
    }
}
