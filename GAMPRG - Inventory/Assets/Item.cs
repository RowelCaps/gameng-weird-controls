﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Item : MonoBehaviour
{
    [Header("Item Data")]
    [SerializeField] protected string itemName;         //Name of item
    [SerializeField] private Sprite itemLogo;           //Ui could use this variable ----

    [Header("Item Settings")]
    [SerializeField] private int maxStack = 3;          //Max Stack of the item   ----
    [SerializeField] private bool isStackable = true;   //Bool for stackables     ---- They're private, can be only set at inspector and cannot be accesed in child class
   //this is the current inventory the item is on

    [Header("Item Inventory Events")]
    public UnityEvent OnItemAdded;                     //this event will be invoke once the item has been added to the inventory
    public UnityEvent OnItemDiscarded;                 //event will run when the item is either discarded,used or removed from the inventory

    [HideInInspector] public Inventory currentInventoryStashed = null;

    public string ItemName { get { return itemName; } }
    public int GetMaxStack { get { return maxStack; } }
    public bool IsStackable { get { return isStackable; } }
    public Sprite GetItemSprite { get { return itemLogo; } }
}

//Implement this interface if you want to make your item usable
public interface IUsableItem
{
    void OnUseItem(Inventory user);
}
