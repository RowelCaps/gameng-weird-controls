﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTest : Item, IUsableItem
{
    [SerializeField] private int healAmount = 15;
    [SerializeField] private Inventory mask;
    [SerializeField] Item pref;
    [SerializeField] bool test = true;

    private void Update()
    {
        if (test)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("space desu");
            mask.AddItem(pref);
        }

        if (Input.GetKeyDown(KeyCode.E))
            mask.DiscardAllItemAtIndexOf(mask.GetOccupiedSlotCount -1);

        if (Input.GetKeyDown(KeyCode.Q))
            mask.UseAllItemWithNameOf("ItemTest");
    }

    public void OnUseItem(Inventory user)
    {
        user.GetComponent<Health>().AddHealth(healAmount);
    }
}
