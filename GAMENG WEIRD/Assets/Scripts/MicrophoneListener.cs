﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using UnityEngine.Events;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneListener : MonoBehaviour
{
    [Header("List of words to recognize")]
    [SerializeField] private List<string> wordToListen;

    [Header("Target Decibels level")]
    [SerializeField, Range(0, 1)] private float targetDb = 0.5f;
    [SerializeField] private float targetDecibelProgression = 0.1f;

    [Header("Minimum Decibel reading settings")]
    [SerializeField, Range(0,1)] private float minDb = 0.1f;

    [Header("Time handler when speech has been recognize")]
    [SerializeField] private float timeDecibelsOnRecognizeSpeech = 0.5f;
    [SerializeField] private float timeErrorCatcherOnRecognizeSpeed = 1;

    [Header("Events for when speech is recognize")]
    [SerializeField] private UnityEvent OnSpeechRecognize;
    [SerializeField] private UnityEvent OnSpeechFailRecognize;
    [SerializeField] private UnityEvent OnFinishLevel;

    string mic;
    private AudioClip src;
    private float currentTime = 0;
    private float errorCounter = 0;
    KeywordRecognizer keywordRecognizer;
    private List<float> Decibels = new List<float>();

    public float GetTargetDecibels { get { return targetDb; } }

    private void Awake()
    {
        mic = Microphone.devices[0];
        src = Microphone.Start(mic, true, 999, 4410);
    }

    void Start()
    {
        keywordRecognizer = new KeywordRecognizer(wordToListen.ToArray());
        keywordRecognizer.OnPhraseRecognized += RecognizeSpeech;
        keywordRecognizer.Start();
    }

    private void Update()
    {
        if(errorCounter > 0 && GetMicDecibels() <= minDb)
        {
            errorCounter -= Time.deltaTime;
            return;
        }

        if (GetMicDecibels() <= minDb || timeDecibelsOnRecognizeSpeech < currentTime)
        {
            Decibels.Clear();
            currentTime = 0;
            return;
        }

        errorCounter = timeErrorCatcherOnRecognizeSpeed;
        currentTime += Time.deltaTime;
        Decibels.Add(GetMicDecibels());
    }

    private void RecognizeSpeech(PhraseRecognizedEventArgs speech)
    {
        float largestDb = float.MinValue;

        foreach(float db in Decibels)
        {
            if (db > largestDb)
                largestDb = db;
        }

        if(1f - targetDecibelProgression <= targetDb)
        {
            OnFinishLevel.Invoke();
            return;
        }


        if (largestDb >= targetDb)
            OnSpeechRecognize.Invoke();
        else
            OnSpeechFailRecognize.Invoke();
    }

    public float GetMicDecibels()
    {
        float levelMax = 0;
        float[] waveData = new float[128];
        int micPos = Microphone.GetPosition(null) - (128 + 1);

        if (micPos < 0)
            return 0;

        src.GetData(waveData, micPos);

        for(int i = 0; i < 128; i++)
        {
            float wave = waveData[i] * waveData[i];

            if (levelMax < wave)
                levelMax = wave;
        }

        return levelMax;
    }

    public void ProgressTargetDb()
    {
        targetDb += targetDecibelProgression;
        targetDb = Mathf.Clamp(targetDb, 0, 1);
    }
}
