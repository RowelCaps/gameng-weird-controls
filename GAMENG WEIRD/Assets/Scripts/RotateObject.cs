﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    [SerializeField] private float rotationSpeed;
    [SerializeField] private Vector3 speedDirection;

    // Update is called once per frame
    private void Start()
    {
        rotationSpeed = Random.Range(0, 100) <= 50 ? rotationSpeed * -1 : rotationSpeed;
        rotationSpeed = Random.Range(0.1f, rotationSpeed);
    }

    void Update()
    {
        transform.Rotate(Vector3.forward * rotationSpeed);
    }
}
