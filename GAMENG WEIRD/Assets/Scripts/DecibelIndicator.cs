﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecibelIndicator : MonoBehaviour
{
    [SerializeField] private MicrophoneListener listener;
    [Header("Objects to determine where the target should be")]
    [SerializeField] private Image background;
    [SerializeField] private Image targetUp;
    [SerializeField] private Image targetDown;

    [Header("Target bar setting")]
    [SerializeField] private float padding;

    private Image img;

    private void Awake()
    {
        UpdateTargetDb();
        img = GetComponent<Image>();
    }

    private void Update()
    {
        img.fillAmount = listener.GetMicDecibels();
    }

    public void UpdateTargetDb()
    {
        float targetDb = listener.GetTargetDecibels;
        targetDown.color = background.color;
        targetUp.fillAmount = targetDb + padding;
        targetDown.fillAmount = targetDb;
    }
}
